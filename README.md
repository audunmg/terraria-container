# Container image for Terraria game server.



## Run it like this:

```
mkdir /volume/terraria/config /volume/terraria/world
chown 7777:7777 /volume/terraria/config /volume/terraria/world
docker run -it --rm --publish 7777:7777 --volume /volume/terraria/config:/config --volume /volume/terraria/world:/world terraria
```

To configure, either use uppercase environment variable and default serverconfig will use that, or simply drop in a serverconfig.txt.

ex, --env WORLD=myworld.wld --env WORLDNAME="My World"

Default serverconfig.txt:
```
#Load a world and automatically start the server.
world=/world/world.wld
#Sets the folder where world files will be stored
worldpath=/world
#Sets the name of the world when using autocreate
worldname=Terraria
#Creates a new world if none is found. World size is specified by: 1(small), 2(medium), and 3(large).
autocreate=2
#Sets the difficulty of the world when using autocreate 0(classic), 1(expert), 2(master), 3(journey)
difficulty=0

#Sets the max number of players allowed on a server.  Value must be between 1 and 255
maxplayers=16
#Set the port number
port=7777
#Set the server password
password=

#Set the message of the day
motd=Welcome to Terraria!
#The location of the banlist. Defaults to "banlist.txt" in the working directory.
banlist=/config/banlist.txt
#Adds addition cheat protection.
secure=1
#Sets the server language from its language code.
#English = en-US, German = de-DE, Italian = it-IT, French = fr-FR, Spanish = es-ES, Russian = ru-RU, Chinese = zh-Hans, Portuguese = pt-BR, Polish = pl-PL,
language=en-US
#Reduces enemy skipping but increases bandwidth usage. The lower the number the less skipping will happen, but more data is sent. 0 is off.
npcstream=0
# Journey mode power permissions for every individual power. 0: Locked for everyone, 1: Can only be changed by host, 2: Can be changed by everyone
journeypermission_time_setfrozen=1
journeypermission_time_setdawn=1
journeypermission_time_setnoon=1
journeypermission_time_setdusk=1
journeypermission_time_setmidnight=1
journeypermission_godmode=1
journeypermission_wind_setstrength=1
journeypermission_rain_setstrength=1
journeypermission_time_setspeed=1
journeypermission_rain_setfrozen=1
journeypermission_wind_setfrozen=1
journeypermission_increaseplacementrange=1
journeypermission_setdifficulty=1
journeypermission_biomespread_setfrozen=1
journeypermission_setspawnrate=1
```

Heavily inspired by [kaysond/docker-terraria](https://github.com/kaysond/docker-terraria) and [ryansheehan/terraria](https://github.com/ryansheehan/terraria).

I created this because I wanted it to run without privileges by default and fully configurable by environment variables for Kubernetes (but also works for Docker Compose).

The volumes need to be set `chown 7777:7777 <PATH TO VOLUME>` before launching, because the server runs as that user instead of root. Since the serverconfig can be entirely configured in environment variables, /config could just be tmpfs if you just set banlist to /world/banlist.txt, and only one persistent volume is required.

The rest of the container should be readonly as it should not have permissions to write anywhere else anyway.

stdin_open and tty is required because Terraria crashes if stdin isn't a tty.

To make administration easier, it runs in a screen, so you can attach with kubectl or docker `exec -it <name> screen -x` and detach with ^A-d (that's control-a followed by 'd').

## Docker compose example:

```
version: '2.0'

services:
  terraria:
    container_name: terraria-server
    image: audunmg/terraria-container:latest
    environment:
      WORLDNAME: "Terraria Server Name"
      MOTD: "Welcome to the world!"
    restart: always
    stdin_open: true
    tty: true
    ports:
      - 7777:7777
    volumes:
      - <PATH TO WORLDS>:/world
      - <PATH TO CONFIG>:/config
```
