FROM docker.io/library/alpine:3.16

ARG version="1449"

RUN \
 echo "Install Mono" && \
 echo "@testing http://dl-4.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories && \
 apk add screen mono@testing && \
 rm -rf /var/cache/apk && \
 echo "Install Terraria" && \
 set -x && \
 wget "https://terraria.org/api/download/pc-dedicated-server/terraria-server-${version}.zip" -O "/tmp/terraria-server-${version}.zip" && \
 unzip /tmp/terraria-server-${version}.zip  ${version}/Linux/*  -d /tmp/ && \
 chmod +x /tmp/${version}/Linux/TerrariaServer /tmp/${version}/Linux/TerrariaServer.bin.x86_64 && \
 echo "Delete system.dll because it's out of sync " && \
 rm -f /tmp/${version}/Linux/System.dll && \
 mv  /tmp/${version}/Linux /opt/TerrariaServer && \
 mkdir /config /world && \
 echo "Installed successfully" && \
 adduser -D -u 7777 -g terraria terraria

COPY entrypoint.sh /entrypoint.sh

EXPOSE 7777/tcp

VOLUME [ "/world", "/config" ]

ENV CONFIG="/config/serverconfig.txt"
ENV WORLD="world.wld"
ENV WORLDPATH="/world"
ENV MAXPLAYERS=16

USER terraria:terraria
WORKDIR /config
ENTRYPOINT [ "/bin/sh", "/entrypoint.sh" ]
