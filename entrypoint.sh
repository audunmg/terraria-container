#!/bin/sh

createconfig(){
echo "
#Load a world and automatically start the server.
world=${WORLDPATH:=/world}/${WORLD:=world.wld}
#Sets the folder where world files will be stored
worldpath=${WORLDPATH:=/world}
#Sets the name of the world when using autocreate
worldname=${WORLDNAME:=Terraria}
#Creates a new world if none is found. World size is specified by: 1(small), 2(medium), and 3(large).
autocreate=${AUTOCREATE:=2}
#Sets the difficulty of the world when using autocreate 0(classic), 1(expert), 2(master), 3(journey)
difficulty=${DIFFICULTY:=0}

#Sets the max number of players allowed on a server.  Value must be between 1 and 255
maxplayers=${MAXPLAYERS:=16}
#Set the port number
port=${PORT:=7777}
#Set the server password
password=${PASSWORD:=}

#Set the message of the day
motd=${MOTD:=Welcome to Terraria!}
#The location of the banlist. Defaults to \"banlist.txt\" in the working directory.
banlist=${BANLIST:=/config/banlist.txt}
#Adds addition cheat protection.
secure=${SECURE:=1}
#Sets the server language from its language code.
#English = en-US, German = de-DE, Italian = it-IT, French = fr-FR, Spanish = es-ES, Russian = ru-RU, Chinese = zh-Hans, Portuguese = pt-BR, Polish = pl-PL,
language=${LANGUAGE:=en-US}
#Reduces enemy skipping but increases bandwidth usage. The lower the number the less skipping will happen, but more data is sent. 0 is off.
npcstream=${NPCSTREAM:=0}
#Journey mode power permissions for every individual power. 0: Locked for everyone, 1: Can only be changed by host, 2: Can be changed by everyone
journeypermission_time_setfrozen=${JOURNEYPERMISSION_TIME_SETFROZEN:=1}
journeypermission_time_setdawn=${JOURNEYPERMISSION_TIME_SETDAWN:=1}
journeypermission_time_setnoon=${JOURNEYPERMISSION_TIME_SETNOON:=1}
journeypermission_time_setdusk=${JOURNEYPERMISSION_TIME_SETDUSK:=1}
journeypermission_time_setmidnight=${JOURNEYPERMISSION_TIME_SETMIDNIGHT:=1}
journeypermission_godmode=${JOURNEYPERMISSION_GODMODE:=1}
journeypermission_wind_setstrength=${JOURNEYPERMISSION_WIND_SETSTRENGTH:=1}
journeypermission_rain_setstrength=${JOURNEYPERMISSION_RAIN_SETSTRENGTH:=1}
journeypermission_time_setspeed=${JOURNEYPERMISSION_TIME_SETSPEED:=1}
journeypermission_rain_setfrozen=${JOURNEYPERMISSION_RAIN_SETFROZEN:=1}
journeypermission_wind_setfrozen=${JOURNEYPERMISSION_WIND_SETFROZEN:=1}
journeypermission_increaseplacementrange=${JOURNEYPERMISSION_INCREASEPLACEMENTRANGE:=1}
journeypermission_setdifficulty=${JOURNEYPERMISSION_SETDIFFICULTY:=1}
journeypermission_biomespread_setfrozen=${JOURNEYPERMISSION_BIOMESPREAD_SETFROZEN:=1}
journeypermission_setspawnrate=${JOURNEYPERMISSION_SETSPAWNRATE:=1}
" >  "$CONFIG"
}
startserver(){
if [[ ! -r "${CONFIG:=/config/serverconfig.txt}" ]]; then
	echo "No serverconfig.txt found. Creating one"
	createconfig
fi
screen -d -m -S terraria  mono --server --gc=sgen -O=all /opt/TerrariaServer/TerrariaServer.exe -config "${CONFIG}" -noupnp
}

stopserver(){
    echo stop
    screen -S terraria -X stuff "exit\n"
}

trap stopserver HUP

startserver
sleep infinity
